<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
  <link rel="stylesheet" href="/css/app.css">

  <title>Forum App</title>
</head>

<body>
  <div id="app">
    <router-view></router-view>
  </div>

  <script>
    window.Laravel = <?php echo json_encode([
                      'csrfToken' => csrf_token(),
                    ]); ?>
  </script>
  <script src="/js/app.js"></script>
  <script src="https://use.fontawesome.com/b3742b4a30.js"></script>
</body>

</html>