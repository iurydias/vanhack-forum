import VueRouter from 'vue-router'

let routes = [
  {
    path: '/',
    component: require('./components/Home'),
    children: [
      {
        path: '/forum',
        component: require('./components/Posts')
      }
    ]
  }
]

export default new VueRouter({
  routes,
  linkActiveClass: 'is-active'
})
