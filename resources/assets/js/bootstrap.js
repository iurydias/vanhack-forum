import 'babel-polyfill'

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import axios from 'axios'
import lodash from 'lodash'

window.Vue = Vue
Vue.use(Vuex)
Vue.use(VueRouter)

window.axios = axios
window._ = lodash

window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'