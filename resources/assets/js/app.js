import './bootstrap'
import router from './routes'
import { store } from './store'

import Vuex from 'vuex'

import 'bulma/css/bulma.css'
import '../sass/app.scss'

new Vue({
  el: '#app',
  router,
  store
})
