import Vuex from 'vuex'

export const store = new Vuex.Store({
  state: {
    user: 0,
    users: [],
    categories: []
  },
  actions: {},
  mutations: {
    setUser (state, userId) {
      state.user = userId
    },
    setUsers (state, users) {
      state.users = users
    },
    setCategories (state, categories) {
      state.categories = categories
    }
  },
  getters: {
    getCategory: state => categoryId => {
      for (const category of state.categories) {
        if (category.id === categoryId) return category
      }
    },
    getUser: state => userId => {
      for (const user of state.users) {
        if (user.id === userId) return user
      }
    },
  }
})