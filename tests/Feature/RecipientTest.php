<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RecipientTest extends TestCase
{
    public function testGetRecipients()
    {
        $this
        ->get('recipients')
        ->assertSuccessful();
    }

    public function testGetRecipientsVouchers()
    {
        $this
        ->get('recipients/1/vouchers')
        ->assertSuccessful();

        $this
        ->get('recipients/0/vouchers')
        ->assertStatus(404);
    }
}