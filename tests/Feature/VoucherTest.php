<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VoucherTest extends TestCase
{
    public function testGetVouchers()
    {
        $this
        ->get('vouchers?page=1&search=')
        ->assertSuccessful();
    }
    
    public function testValidateVoucherWithEmptyParameters()
    {
        $this
        ->get('vouchers/validateVoucher?email=&code=')
        ->assertSuccessful()
        ->assertExactJson([
        'message' => 'Invalid information. Please verify email or voucher code.'
        ]);
    }
    
    public function testValidateVoucherWithInvalidParameters()
    {
        $this
        ->get('vouchers/validateVoucher?email=0&code=0')
        ->assertSuccessful()
        ->assertExactJson([
        'message' => 'Invalid information. Please verify email or voucher code.'
        ]);
    }
    
    public function testGenerateVouchersWithEmptyParameters()
    {
        $payload = ['expiration_date' => '', 'id_special_offer' => ''];
        
        $this
        ->json('POST', 'vouchers/generate', $payload)
        ->assertStatus(500);
    }
}