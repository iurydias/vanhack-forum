<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('master');
});

Route::post('login', 'AuthenticationController@authenticate');

Route::get('posts', 'PostController@index');
Route::get('posts/{post}/comments', 'PostController@comments');
Route::post('posts', 'PostController@store');
Route::put('posts/{post}', 'PostController@update');
Route::delete('posts/{post}', 'PostController@delete');

Route::get('categories', 'CategoryController@index');
Route::get('categories/{category}/posts', 'CategoryController@posts');

Route::get('users', 'UserController@index');
Route::get('users/{user}/posts', 'UserController@posts');
Route::get('users/{user}/comments', 'UserController@comments');

Route::post('comments', 'CommentController@store');
Route::put('comments/{comment}', 'CommentController@update');
Route::delete('comments/{comment}', 'CommentController@delete');