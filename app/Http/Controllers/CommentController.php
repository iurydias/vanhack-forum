<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::all();
    }

    public function store()
    {
        $comment = new Comment;
        $comment->content = request('content');
        $comment->user_id = request('user_id');
        $comment->post_id = request('post_id');
        $comment->date = Carbon::now();
        $comment->save();

        return $comment;
    }

    public function update(Comment $comment)
    {
        $comment->content = request('content');
        $comment->save();

        return $comment;
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
    }
}
