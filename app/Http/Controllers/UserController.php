<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function posts(User $user)
    {
        return $user->posts()->get();
    }

    public function comments(User $user)
    {
        return $user->comments()->get();
    }
}
