<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;

class PostController extends Controller
{
    public function index()
    {
        return Post::all();
    }

    public function store()
    {
        $post = new Post;
        $post->title = request('title');
        $post->content = request('content');
        $post->user_id = request('user_id');
        $post->category_id = request('category_id');
        $post->date = Carbon::now();
        $post->save();

        return $post;
    }

    public function update(Post $post)
    {
        $post->title = request('title');
        $post->content = request('content');
        $post->category_id = request('category_id');
        $post->save();

        return $post;
    }

    public function delete(Post $post)
    {
        $post->delete();
    }

    public function comments(Post $post) {
        return $post->comments()->get();
    }
}
