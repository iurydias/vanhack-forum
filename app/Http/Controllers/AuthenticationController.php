<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthenticationController extends Controller
{
    public function authenticate()
    {
        $user = User::where([
            ['name', 'LIKE', request('name')],
            ['password', 'LIKE', request('password')]
        ])->first();

        return $user == null ? "Incorrect credentials" : $user;
    }
}
